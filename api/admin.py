from django.contrib import admin
from .models import Friend,Belonging,Borrowed,User
# Register your models here.

@admin.register(Friend)
class FriendAdmin(admin.ModelAdmin):
    pass

@admin.register(Belonging)
class BelongingAdmin(admin.ModelAdmin):
    pass

@admin.register(Borrowed)
class BorrowedAdmin(admin.ModelAdmin):
    pass

# @admin.register(User)
# class UserAdmin(admin.ModelAdmin):
#     pass