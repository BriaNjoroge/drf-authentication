from django.shortcuts import render
#from rest_framework.response import response
from django.http import HttpResponse, JsonResponse
from rest_framework import viewsets, permissions
from rest_framework.views import APIView

from api.permissions import IsOwner
from . import models
from . import serializers
from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

class FriendViewset(viewsets.ModelViewSet):
    queryset = models.Friend.objects.all()
    serializer_class = serializers.FriendSerializer
    permission_classes = [IsOwner]


class BelongingViewset(viewsets.ModelViewSet):
    queryset = models.Belonging.objects.all()
    serializer_class = serializers.BelongingSerializer

class BorrowedViewset(viewsets.ModelViewSet):
    queryset = models.Borrowed.objects.all()
    serializer_class = serializers.BorrowedSerializer

class Listuser(APIView):
    def get(self, request, format=None):
        snippets = models.User.objects.all()
        serializer = serializers.RegistrationSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = serializers.RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.DjangoModelPermissions]

