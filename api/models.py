from django.db import models
from django.contrib.auth.models import User

class OwnedModel(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Friend(models.Model):
    name = models.CharField(max_length=100)

class Belonging(OwnedModel):
    name = models.CharField(max_length=100)

class Borrowed(models.Model):
    what = models.ForeignKey(Belonging, on_delete=models.CASCADE)
    to_who = models.ForeignKey(Friend, on_delete=models.CASCADE)
    when = models.DateTimeField(auto_now_add=True)
    returned = models.DateTimeField(null=True, blank=True)

# class User(models.Model):
#     username = models.CharField(max_length=10)
#     password  = models.CharField(max_length=10)